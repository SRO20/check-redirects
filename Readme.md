This maven project tests URL redirects as below - 
===============================================
1. Reads the From URL from input excel
2. Opens Chrome and hits the from URL
3. Fetches the URL redirected to from browser
4. Compares and reports against the expected To URL from the excel.

The above process repeats for all the rows within the excel.

Pre-requisites - 
===============
1. Please have the From and To URLs within a single column in the below format in the excel spread-sheet, separated by ~
https://tickets.racv.com.au//~https://www.racv.com.au/travel-leisure/attraction-tickets.html
2.In case you have them in separate columns, please merge prior feeding it into the class
3. Ensure you have the correct version of the Chromedriver corresponding to the version of Chrome you have in your local
4. Update the Excel file location, Chromedriver location and Report path location in the class as per your local if needed.
   Sample files can be found within src/test/resources folder
   
Execution - 
==========
1. Clone the project to your local.
2. Build as standard maven project
3. Execute TestRedirect class as java application
4. Extent report is generated and can be found in the src/test/resources/report package