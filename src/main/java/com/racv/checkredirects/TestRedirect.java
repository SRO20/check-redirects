package com.racv.checkredirects;

import org.apache.poi.ss.formula.functions.Column;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

/**
 * This tests URL redirects from an excel file as input
 * @author Sumanta Roy
 * @date 13 Nov 19
 *
 */
public class TestRedirect {
	
	//URL repository excel path. 
	//NOTE - Update this as per your local
	private static final String FILE_NAME = "./src/test/resources/data/UrlTestSheet1.xlsx";
	
	

	public static void main(String[] args) throws InterruptedException {
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.setAcceptInsecureCerts(true);
		chromeOptions.addArguments("--no-sandbox");
		chromeOptions.addArguments("--disable-dev-shm-usage");
		chromeOptions.addArguments("--headless");
		chromeOptions.addArguments("--disable-gpu");
		chromeOptions.addArguments("--window-size=1440,900");
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		
		//Set chromedriver path. 
		//NOTE - Update this as per your local
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver(chromeOptions);
		
		//Set report file path. 
		//NOTE - Update this as per your local
		String extentReportFile = "./src/test/resources/report/extentReportFile.html";
		// Create object of extent report and specify the report file path.
		ExtentReports extent = new ExtentReports(extentReportFile, false);
		// Start the test using the ExtentTest class object.
		ExtentTest extentTest = extent.startTest("TestRedirect",
				"Test URL redirects");

        try {
        	
            FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
            Workbook workbook = new XSSFWorkbook(excelFile);
            //From URL
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
            
            //Iterate over all the rows in the excel spreadsheet
            while (iterator.hasNext()) {

                Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();
                int rowNumber = currentRow.getRowNum();
                

                while (cellIterator.hasNext()) {

                    Cell currentCell = cellIterator.next();
                    System.out.println("Cell value >>"+currentCell.getStringCellValue());
                    String[] fromURlArr = currentCell.getStringCellValue().split("~");
                    String fromURl = fromURlArr[0];
                    System.out.println("fromURl>>"+fromURlArr[0]);
                    String expectedToURl = fromURlArr[1]; 
                    System.out.println("expectedToURl>>"+fromURlArr[1]);
                    //Render from URL
                    driver.get(fromURl);
                    
                    //Get the actual redirected URL from browser and trim any filter in URL
                    String actualToUrl = driver.getCurrentUrl();
                    String [] actualToUrlArr =  actualToUrl.split("\\?");
                    System.out.println("actualToUrl>>"+actualToUrl);
                    System.out.println("actualToUrlArr>>"+actualToUrlArr[0]);
                    
                    //Get the page title redirected to
                    String pageTitle = driver.getTitle();
                    System.out.println("pageTitle>>"+pageTitle);
                    
                    //compare
                    if (actualToUrlArr[0].contentEquals(expectedToURl) && !pageTitle.contains("404")) {
                    	System.out.println("Actual and expected URLs match");
                    	extentTest.log(LogStatus.PASS, "Row Number - "+rowNumber+">>>> Actual and expected URLs match!! fromURl>>>>"+fromURl+">>>> expectedToURl>>>>"+expectedToURl+">>>>actualToUrl>>"+actualToUrl);
                    	//driver.close();
                    }else {
                    	System.out.println("Actual and expected URLs DO NOT match!!");
                    	extentTest.log(
            					LogStatus.FAIL,
            					"Row Number - "+rowNumber+">>>> Actual and expected URLs DO NOT match!!  fromURl>>>>"+fromURl+">>>> expectedToURl>>>>"+expectedToURl+">>>>actualToUrl>>"+actualToUrl+">>>>toPageTitle>>"+pageTitle);
                    }
                    

                }
                System.out.println("Checking Next URL----");
                System.out.println("rowNumber----"+rowNumber);
                rowNumber++;

            }
            //Close browser
            driver.close();
            extent.endTest(extentTest);
			// writing everything to document.
			extent.flush();
        } catch (Exception e) {
        	driver.close();
            extent.endTest(extentTest);
			// writing everything to document.
			extent.flush();
            e.printStackTrace();
        } 

    }

}
